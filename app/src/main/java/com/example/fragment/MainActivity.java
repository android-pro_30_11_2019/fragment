package com.example.fragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.fragment.fragment.ArticleDetailFragment;
import com.example.fragment.fragment.ArticleFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAdd = findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(this);

        showFragment(new ArticleFragment(),false);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAdd:
                showFragment(new ArticleDetailFragment(),true);
                break;
        }
    }

    /**
     * Show Fragment
     * @param fragment
     */
    private void showFragment(Fragment fragment,boolean addTobackStack){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmnent_container,fragment);
        if(addTobackStack){
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }
}
