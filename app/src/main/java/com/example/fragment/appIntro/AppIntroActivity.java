package com.example.fragment.appIntro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Toast;

import com.example.fragment.R;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntro2Fragment;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;

import static android.graphics.BitmapFactory.decodeResource;

public class AppIntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*Style 1*/
        SliderPage sliderPage = new SliderPage();
        sliderPage.setTitle("Page 1");
        sliderPage.setDescription("Description");
        sliderPage.setImageDrawable(R.drawable.ic_launcher_foreground);
        sliderPage.setBgColor(ContextCompat.getColor(this,R.color.colorPrimary));

//        addSlide(AppIntro2Fragment.newInstance(sliderPage));
//        addSlide(AppIntro2Fragment.newInstance(sliderPage));
//        addSlide(AppIntro2Fragment.newInstance(sliderPage));

        /*Style 2*/

        addSlide(AppIntro2Fragment.newInstance("Page 1",
                "Description",
                R.drawable.ic_launcher_foreground,
                ContextCompat.getColor(this,R.color.colorPrimary),
                ContextCompat.getColor(this,R.color.colorPrimaryDark),
                ContextCompat.getColor(this,R.color.colorPrimaryDark)
        )
        );
        addSlide(AppIntro2Fragment.newInstance("Page 1",
                "Description",
                R.drawable.ic_launcher_foreground,
                ContextCompat.getColor(this,R.color.colorPrimary),
                ContextCompat.getColor(this,R.color.colorPrimaryDark),
                ContextCompat.getColor(this,R.color.colorPrimaryDark)
                )
        );

        /*Custom Layout*/
        addSlide(SampleSlide.newInstance(R.layout.app_intro_2));

        setFlowAnimation();

        // Ask for CAMERA permission on the second slide
        askForPermissions(new String[]{Manifest.permission.CAMERA}, 2); // OR

        // This will ask for the camera permission AND the contacts permission on the same slide.
        // Ensure your slide talks about both so as not to confuse the user.
        askForPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_CONTACTS}, 2);
    }
    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        Toast.makeText(this, "Skip was clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Toast.makeText(this, "Done was clicked", Toast.LENGTH_SHORT).show();
    }
}
