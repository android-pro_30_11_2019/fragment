package com.example.fragment.appIntro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import com.example.fragment.R;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntro2Fragment;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;

public class AppIntroTestActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlide(AppIntro2Fragment.newInstance("Page 1",R.font.courierprime_bold,"Description",R.font.test,R.drawable.ic_launcher_foreground,ContextCompat.getColor(this,R.color.colorPrimary),0,0));
        addSlide(AppIntro2Fragment.newInstance("Page 2",R.font.test,"Description",R.font.test,R.drawable.ic_launcher_foreground,ContextCompat.getColor(this,R.color.colorPrimary),0,0));
        addSlide(AppIntro2Fragment.newInstance("Page 3","Description",R.drawable.ic_launcher_foreground,ContextCompat.getColor(this,R.color.colorPrimary),0,0));
        //addSlide(AppIntro2Fragment.newInstance("Page 2","Description",R.drawable.ic_launcher_foreground,ContextCompat.getColor(this,R.color.colorPrimary),0,0));
        //addSlide(AppIntro2Fragment.newInstance("Page 3","Description",R.drawable.ic_launcher_foreground,ContextCompat.getColor(this,R.color.colorPrimary),0,0));
    }
}
